# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.


class User(AbstractUser):
    First_Name = models.CharField(max_length=100)
    Last_Name = models.CharField(max_length=100)
    Gender = models.CharField(max_length=255)
    email = models.CharField(max_length=100, primary_key=True)
    Phone_Number = models.CharField(max_length=255)
    OTP = models.CharField(max_length=255)

class city(models.Model):
    Name = models.CharField(max_length=255)
    CountryCode = models.CharField(max_length=255)
    District = models.CharField(max_length=255)
    Population = models.IntegerField()

class country(models.Model):
    Code = models.CharField(max_length=255,primary_key=True)
    Name = models.CharField(max_length=255)
    Continent = models.CharField(max_length=255)
    Region = models.CharField(max_length=255)
    SurfaceArea = models.FloatField()
    IndepYear = models.IntegerField(null=True)
    Population = models.IntegerField()
    LifeExpectancy = models.FloatField(null=True)
    GNP = models.FloatField(null=True)
    GNPOld = models.FloatField(null=True)
    LocalName = models.CharField(max_length=255)
    GovernmentForm = models.CharField(max_length=255)
    HeadOfState = models.CharField(max_length=255,null=True)
    Capital = models.IntegerField(null=True)
    Code2 = models.CharField(max_length=255)

class countrylanguage(models.Model):
    CountryCode = models.CharField(max_length=255)
    Language = models.CharField(max_length=255)
    IsOfficial = models.CharField(max_length=255)
    Percentage = models.FloatField()


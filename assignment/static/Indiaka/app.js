angular.module('Indiaka', ['ui.bootstrap', 'ui.router', 'ngAnimate', 'login', 'ngResource']);

angular.module('Indiaka').config(function($stateProvider, $urlRouterProvider) {

    /* Add New States Above */
    $urlRouterProvider.otherwise('/login');

});

angular.module('Indiaka').run(function($rootScope) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

});
